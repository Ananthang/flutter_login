import 'package:flutter/material.dart';

void main() {
  runApp(LoginPage());
}

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Apptimus Login Page",
      home: Scaffold(
        appBar: AppBar(
          title: new Text("Apptimus Login Page"),
          centerTitle: true,
        ),
        body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(16.0),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.center,

            children: [
              Container(
                padding: EdgeInsets.only(top: 60.0),
                child: FlutterLogo(
                  size: 100,
                  textColor: Colors.blue,
                  style: FlutterLogoStyle.markOnly,
                ),
              ),
              // child: new Image.asset('assets/images/logo.jpg')),
              Container(
                padding: EdgeInsets.only(top: 40.0),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter your Email address.',
                    prefixIcon: Icon(Icons.mail),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 20.0),
                child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Enter your password.',
                    prefixIcon: Icon(Icons.lock),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 20.0),
                child: ElevatedButton(
                  style: ButtonStyle(
                    elevation: MaterialStateProperty.resolveWith<double>(
                        (Set<MaterialState> states) {
                      if (states.contains(MaterialState.pressed)) return 16;
                      return null;
                    }),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.blue),
                      ),
                    ),
                    minimumSize:
                        MaterialStateProperty.all(Size(double.infinity, 50)),
                  ),
                  onPressed: () {},
                  child: Text('Login'),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 20.0),
                child: TextButton(
                  style: ButtonStyle(
                    elevation: MaterialStateProperty.resolveWith<double>(
                        (Set<MaterialState> states) {
                      if (states.contains(MaterialState.pressed)) return 16;
                      return null;
                    }),
                  ),
                  onPressed: () {},
                  child: Text('Create an Account'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
